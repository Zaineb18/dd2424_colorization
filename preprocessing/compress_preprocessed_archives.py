import glob, os, numpy as np

dir_str = "processed/"

file_paths = glob.glob(os.path.join(dir_str, '*.npz'))
for i in range(len(file_paths)):
    with np.load(file_paths[i]) as f:
        ab = f['binned_ab'].astype(np.float32)
        l = f['lightness'].astype(np.uint8)
    np.savez_compressed("{}s.npz".format(file_paths[i][:-4]), binned_ab = ab, lightness = l)
    
