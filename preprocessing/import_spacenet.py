import glob
import os

import numpy as np
from osgeo import gdal
from PIL import Image


def get_dataset(file):
    dataset = gdal.Open(file)
    return dataset


def convert_GTiff_to_RGB(data, to_uint8, max_bands, min_bands, max_percent=98):
    assert(len(data.shape) == 4)

    if to_uint8:
        max_value = 255
    else:
        max_value = 1.0

    data = data.astype('float32', casting='unsafe', copy=False)

    for i in range(3):
        band = data[:, :, :, i]
        max_band = max_bands[i]
        min_band = min_bands[i]

        band[band >= max_band] = max_band
        band[band <= min_band] = min_band
        scale_ratio = max_value / (max_band - min_band)
        data[:, :, :, i] = (band - min_band) * scale_ratio

    if to_uint8:
        data = data.astype('uint8', casting='unsafe', copy=False)

    return data, max_bands


def calculate_max_min_bands_batch(data, max_percent=100):
    assert(len(data.shape) == 4)

    data = data.astype('float32', casting='unsafe', copy=False)

    print("\nCalculating max per band for data of shape: " + str(data.shape))

    max_bands = np.zeros(3)
    min_bands = np.zeros(3)
    for i in range(3):
        band = data[:, :, :, i]
        min_bands[i] = np.percentile(band, 1)
        max_bands[i] = np.percentile(band, max_percent)

    print("The max per band is:" + str(max_bands))
    print("The min per band is:" + str(min_bands))

    return max_bands, min_bands


def calculate_max_bands_for_dir(dir_str):
    file_paths = glob.glob(os.path.join(dir_str, '*.tif'))
    data_list = []
    max_bands_final = np.zeros(3)
    min_bands_final = np.zeros(3)
    for i in range(len(file_paths)):
            dataset = get_dataset(file_paths[i])
            data_single = dataset.ReadAsArray().transpose(1, 2, 0)
            dataset = None

            data_flat = data_single.reshape(data_single.shape[1] * data.shape[2], data.shape[3])
            if (data_flat[:, 0] + data_flat[:, 1] + data_flat[:, 2] < 3).sum() / data_flat.shape[0] > 0.1:
                continue

            data_list.append(data_single)

            if len(data_list) == 200 or i == len(file_paths) - 1:
                data = np.array(data_list)
                max_bands, min_bands = calculate_max_min_bands_batch(data)
                max_bands_final += max_bands * data.shape[0] / len(file_paths)
                min_bands_final += min_bands * data.shape[0] / len(file_paths)

                data_list = []

    print("Final max per band is:" + str(max_bands_final))
    print("Final min per band is:" + str(min_bands_final))
    return max_bands_final, min_bands_final


def convert_images_in_dir(dir_str, out_str, max_bands=None, min_bands=None):
    directory = os.fsencode(dir_str)

    filenames = [os.fsdecode(file) for file in os.listdir(directory) if os.fsdecode(file).endswith('.tif')]
    print("\nConverting %d files" % len(filenames))
    data_list = []
    for i in range(len(filenames)):
            in_path = os.path.join(dir_str, filenames[i])
            dataset = get_dataset(in_path)
            data = np.array([dataset.ReadAsArray().transpose(2, 1, 0)])
            dataset = None

            data_flat = data.reshape(data.shape[1] * data.shape[2], data.shape[3])
            if (data_flat[:, 0] + data_flat[:, 1] + data_flat[:, 2] < 3).sum() / data_flat.shape[0] > 0.1:
                continue

            data_out, max_bands = convert_GTiff_to_RGB(data,
                                                       to_uint8=True,
                                                       max_bands=max_bands,
                                                       min_bands=min_bands)
            # data_list.append(data_out[0])

            out_path = os.path.join(out_str, "{}.png".format(filenames[i][:-4]))
            out_path_thumbs = os.path.join(out_str, os.path.join("thumbs", "{}.png".format(filenames[i][:-4])))
            print("\t %s => %s" % (in_path, out_path))

            im = Image.fromarray(data_out[0], mode='RGB')
            im.save(out_path)
            im.thumbnail((256,256), Image.ANTIALIAS)
            
            im.save(out_path_thumbs)

            # if len(data_list) == 200 or i == len(file_paths) - 1:
            #    data_out_array = np.array(data_list)
            #    np.savez_compressed(os.path.join(out_str, 'converted_data_{}.npy'.format(i)), data=data_out_array)


dir_str = "/home/shared/spacenet/AOI_4_Shanghai_Train/RGB-PanSharpen/"
out_str = "/home/shared/output/"

max_bands, min_bands = calculate_max_bands_for_dir(dir_str)
print(max_bands)
#max_bands = np.array([452., 472.9, 365.])
min_bands = np.array([0., 0., 0.])
convert_images_in_dir(dir_str, out_str, max_bands=max_bands, min_bands=min_bands)
