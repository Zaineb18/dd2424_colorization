import os
from PIL import Image

def image_to_thumbs(dir_str, out_str):
    directory = os.fsencode(dir_str)
    filenames = [os.fsdecode(file) for file in os.listdir(directory) ]
    data_list = []
    for i in range(len(filenames)):
        try:
            in_path = os.path.join(dir_str, filenames[i])
            im = Image.open(in_path)
            w, h = im.size
            if w < 256 or h < 256:
                continue
            if w < h:
                diff = h-w
                start = diff // 2
                im = im.transform((256, 256), Image.EXTENT, data=(0, start, w, start + w))
            if h <= w:
                diff = w - h
                start = diff // 2
                im = im.transform((256, 256), Image.EXTENT, data=(start, 0, start + h, h))
            if not im.mode == "RGB":
                im = im.convert(mode="RGB")
            out_path_thumbs = os.path.join(out_str,filenames[i])
            im.save(out_path_thumbs, format='png')
        except:
            print(filenames[i])


if __name__ == '__main__':
    dir_str = "/home/shared/imagenet_raw/jpeg"
    dir_out = "/home/shared/output/imagenet_thumbs"
    image_to_thumbs(dir_str, dir_out)
