import glob
import os

import numpy as np

from PIL import Image
from skimage import color
from sklearn.neighbors import NearestNeighbors


NUM_BINS_PER_DIM = 22
MIN_BIN_VALUE = -110
MAX_BIN_VALUE = 110

BIN_TO_INDEX_MAP = np.load('/home/shared/colorization/resources/bin_map.npy')
AB_OF_CENTERS = np.load('/home/shared/colorization/resources/bin_map_inverse.npy')
NUM_POSSIBLE_BINS = int(np.max(BIN_TO_INDEX_MAP)) + 1
N_NEIGHBORS = 10


def convert_rgb_to_lab(rgb):
    assert(np.max(rgb.flatten()) <= 1.0)
    
    lab = color.rgb2lab(rgb)
    lightness = lab[:, :, :, 0].astype(np.uint8)
    ab = lab[:, :, :, 1:3].astype(np.float32)
    return lightness, ab


# ab.shape = NxXxYx2
def convert_ab_to_bin_space(ab, knn, sigma):
    assert(len(ab.shape) == 4)

    number_of_pixels = ab.shape[0] * ab.shape[1] * ab.shape[2]

    dist, idx = knn.kneighbors(ab.reshape([number_of_pixels, 2]))
    # number_of_pixels x N_NEIGHBORS
    # weight according to gaussian of distance
    weights = np.exp(-dist ** 2 / (2 * sigma ** 2))
    weights = weights / np.sum(weights, axis=1)[:, np.newaxis]

    # already flattened
    weighted_bins = np.zeros([number_of_pixels, NUM_POSSIBLE_BINS])
    weighted_bins[np.arange(number_of_pixels)[:, np.newaxis], idx] = weights
    weighted_bins = weighted_bins / np.sum(weighted_bins, axis=1)[:, np.newaxis]
    # return back to individual images and pixels
    weighted_bins = weighted_bins.reshape([ab.shape[0], ab.shape[1], ab.shape[2], NUM_POSSIBLE_BINS])

    return weighted_bins


def calculate_prior_distribution(weighted_bins):
    bins_counted = np.sum(weighted_bins, axis=(0, 1, 2))
    bins_counted /= np.sum(bins_counted)
    return bins_counted


def pre_process_batch(rgb_data, knn):
    (lightness, ab) = convert_rgb_to_lab(rgb_data)
    ab = ab[:, ::4, ::4]
    weighted_bins = convert_ab_to_bin_space(ab, knn=knn, sigma=5.)
    prior_distribution = calculate_prior_distribution(weighted_bins)
    return lightness, weighted_bins, prior_distribution


def pre_process_files_in_dir(dir_str):

    print("Initializing k-nearest neighbor with k={} and {} centers.".format(N_NEIGHBORS, NUM_POSSIBLE_BINS))
    knn = NearestNeighbors(n_neighbors=N_NEIGHBORS, algorithm='kd_tree').fit(AB_OF_CENTERS)

    dist, idx = knn.kneighbors(AB_OF_CENTERS)

    file_paths = glob.glob(os.path.join(dir_str, '*'))
    print("Found " + str(len(file_paths)) + " files.")

    data_list = []
    total = 0
    prior_total = np.zeros(NUM_POSSIBLE_BINS)
    batch_size = 20
    
    #count_batch=0
    for i in range(len(file_paths)):
        sample = np.array(Image.open(file_paths[i])).astype('float32', copy=False) / 255.
        data_list.append(sample)
        if len(data_list) == batch_size or i == len(file_paths) - 1:
            data = np.array(data_list)
            total += data.shape[0]

            filename = "processed_samples_{}.npz".format(total)

            if os.path.isfile(os.path.join(out_samples, filename)):
                print("file exists, skipping, {}". format(filename))
                data_list = []
                continue
        

            
            print("Processing data:" + str(data.shape))
            
            lightness, binned_ab, prior_distribution = pre_process_batch(data, knn=knn)
            prior_total += prior_distribution * data.shape[0]
            
            
            np.savez_compressed(os.path.join(out_samples_test, filename), lightness=lightness, binned_ab=binned_ab)
            data_list = []
            # count_batch+=1
            
    prior_total = prior_total / total
    return prior_total


if __name__ == '__main__':

    ## IMAGENET
    dir_str = "/home/shared/imagenet/thumbs"
    out_prior = "/home/shared/imagenet/"
    out_samples = "/home/shared/imagenet/processed"
    out_samples_test = "/home/shared/imagenet/processed_test" 

    ## SPACENET
    #dir_str = "/home/shared/spacenet/thumbs"
    #out_prior = "/home/shared/spacenet"
    #out_samples = "/home/shared/spacenet/processed"

    print(NUM_POSSIBLE_BINS)
    prior = pre_process_files_in_dir(dir_str)
    #np.savez_compressed(out_prior, prior=prior) 
