import numpy as np


def NonGreyMask(ab_images):
	N = ab_images.shape[0]
	out_mask =  np.zeros((N,1, ab_images.shape[2], ab_images.shape[3]))
	thresh=5
	for i in range(0,N):
		out_mask[i,0,:,:]=np.any(ab_images[i,:,:,:]>thresh,axis=0)
	return 
