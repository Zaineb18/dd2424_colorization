import numpy as np
from skimage import color
import scipy.misc
from matplotlib import pyplot as plt
from model.model_definition import init_model, load_data_generator
import scipy.ndimage as ndimage
import os


DIR_STR = ""
INVERSE_BIN_MAP = np.load('resources/bin_map_inverse.npy')

model = init_model(INVERSE_BIN_MAP.shape[0], False)
model.load_weights("model_imagenet_v3_9760.h5", by_name=True)

image_index = 0
for ([lightness, corr_factor], ab_quantized) in load_data_generator(DIR_STR, 10):
    MIN_INDEX = 0
    MAX_INDEX = lightness.shape[0]

    lightness = lightness[MIN_INDEX:MAX_INDEX]
    prediction = model.predict(x=[lightness, corr_factor])

    lightness = lightness + 50.
    ab_predicted = prediction @ INVERSE_BIN_MAP

    lab_upscaled = np.zeros([ab_predicted.shape[0], 256, 256, 3])
    lab_upscaled[:, :, :, 0] = lightness.squeeze()

    for i in range(ab_predicted.shape[0]):
        lab_upscaled[i, :, :, 1] = ndimage.zoom(ab_predicted[i, :, :, 0], 4)
        lab_upscaled[i, :, :, 2] = ndimage.zoom(ab_predicted[i, :, :, 1], 4)

    for image in lab_upscaled:
        rgb = color.lab2rgb(image)
        scipy.misc.imsave(os.path.join(DIR_STR, "demo/out{}.png".format(image_index)), rgb)
        image_index += 1

    print("Finished batch")

