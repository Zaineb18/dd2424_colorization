import numpy as np
from skimage import color
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import


# spacing = np.arange(0.0, 1.0, step=0.1)
# (r,g,b) = np.meshgrid(spacing, spacing, spacing)
#
# colors = np.stack([r.flatten(), g.flatten(), b.flatten()]).T
#
#
# lab = color.rgb2lab(colors[np.newaxis, :]).squeeze()
#
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(lab[:, 0], lab[:, 1], lab[:, 2])
# axes = plt.gca()
# axes.set_xlim([-10, 110])
# axes.set_ylim([-110, 110])
# axes.set_zlim([-110, 110])
# ax.set_xlabel('L')
# ax.set_ylabel('a')
# ax.set_zlabel('b')
# plt.show()


def do_spacing(min_val, max_val, steps):
    bin_size = (max_val - min_val) / steps
    return np.linspace(min_val + bin_size/2, max_val - bin_size/2, num=steps)

a = do_spacing(-110, 110, 23)
b = do_spacing(-110, 110, 23)

(a_grid, b_grid) = np.meshgrid(a, b)
l_grid = np.ones(a_grid.shape) * 50

labcolors = np.stack([l_grid, a_grid, b_grid]).transpose(1, 2, 0)

rgb = color.lab2rgb(labcolors)
lab_reconverted = color.rgb2lab(rgb)

in_gamut = (np.abs(lab_reconverted[:, :, 0] - 50) < 1)

output = rgb
output[:, :, 0] = output[:, :, 0] * in_gamut
output[:, :, 1] = output[:, :, 1] * in_gamut
output[:, :, 2] = output[:, :, 2] * in_gamut

plt.imshow(output)
plt.show()