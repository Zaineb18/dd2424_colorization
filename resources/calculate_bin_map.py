import numpy as np
from skimage import color
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import


def adjust_sigmoid(image, min, max, cutoff=0.5, gain=5.5, inv=False):
    image = (image - min) / (max - min)
    corrected = (1 / (1 + np.exp(gain * (cutoff - image))))
    corrected = corrected * (max - min) + min
    return corrected

STEP_SIZE = 10

spacing = np.arange(0.0, 256.0, step=1.0) / 255.0
(r, g, b) = np.meshgrid(spacing, spacing, spacing)
colors = np.stack([r.flatten(), g.flatten(), b.flatten()]).T
lab = color.rgb2lab(colors[np.newaxis, :]).squeeze()

lab[:, 0] = np.power(lab[:, 0] / 100., 1.1) * 100.
lab[:, 1] = adjust_sigmoid(lab[:, 1], min=-110, max=110)
lab[:, 2] = adjust_sigmoid(lab[:, 2], min=-110, max=110)

spacing_bins = np.arange(-110, 111, step=STEP_SIZE)
H, xedges, yedges = np.histogram2d(lab[:, 1], lab[:, 2], spacing_bins)

plt.imshow(H > 0)
plt.show()

good_bins = np.zeros((sum(sum(H > 0)), 2))
bin_map = np.ones(H.shape) * -1

i = 0
for a in range(H.shape[0]):
    for b in range(H.shape[1]):
        if H[a, b] > 0:
            good_bins[i, :] = [xedges[a] + STEP_SIZE / 2, yedges[b] + STEP_SIZE / 2]
            bin_map[a, b] = i
            i += 1

print(i)
np.save('resources/bin_map.npy', bin_map)
np.save('resources/bin_map_inverse.npy', good_bins)


