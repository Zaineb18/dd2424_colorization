from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from model.model_definition import load_prior, init_model, load_data_generator

DIR_STR = "/home/shared/imagenet"
MODEL_NAME = "model_imagenet_v3_9760"
BATCH_SIZE = 40
NUMBER_SAMPLES = 8160

_, num_bins = load_prior(DIR_STR)
model = init_model(num_bins, train=True)

#stop = EarlyStopping(monitor='val_loss',
#                     min_delta=0,
#                    patience=5,
#                     verbose=1,
#                     mode='auto',
#                     baseline=None,
#                     restore_best_weights=True)

save_per_epoch = ModelCheckpoint(MODEL_NAME + "_savepoint.h5", period=1)
steps_per_epoch = NUMBER_SAMPLES // BATCH_SIZE

model.fit_generator(load_data_generator(DIR_STR, BATCH_SIZE),
                    steps_per_epoch=steps_per_epoch,
                    epochs=204,
                    verbose=1,
                    callbacks=[save_per_epoch])

model.save(MODEL_NAME + ".h5")
