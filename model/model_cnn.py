from tensorflow.keras.layers import Conv2D, BatchNormalization, Conv2DTranspose


def make_batch_norm(name):
    return BatchNormalization(name=name,
                              momentum=0.999,
                              epsilon=1e-5)


def make_conv2d(name, filters, kernel_size, strides, dilation_rate):
    return Conv2D(name=name,
                  filters=filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  dilation_rate=dilation_rate,
                  padding='same',
                  activation='relu')


def make_conv2d_transpose(name, filters, kernel_size, strides, dilation_rate):
    return Conv2DTranspose(name=name,
                           filters=filters,
                           kernel_size=kernel_size,
                           strides=strides,
                           dilation_rate=dilation_rate,
                           padding='same',
                           activation='relu')


def init_conv_net(img_input, num_possible_bins):
    # Create the layers and connect them

    # -----CONV1-----
    conv1_1 = make_conv2d(name="conv1_1", filters=64, kernel_size=3, strides=1, dilation_rate=1)(img_input)
    conv1_2 = make_conv2d(name="conv1_2", filters=64, kernel_size=3, strides=2, dilation_rate=1)(conv1_1)
    conv1_bn = make_batch_norm(name="norm1")(conv1_2)

    # -----CONV2-----
    conv2_1 = make_conv2d(name="conv2_1", filters=128, kernel_size=3, strides=1, dilation_rate=1)(conv1_bn)
    conv2_2 = make_conv2d(name="conv2_2", filters=128, kernel_size=3, strides=2, dilation_rate=1)(conv2_1)
    conv2_bn = make_batch_norm(name="norm2")(conv2_2)

    # -----CONV3-----
    conv3_1 = make_conv2d(name="conv3_1", filters=256, kernel_size=3, strides=1, dilation_rate=1)(conv2_bn)
    conv3_2 = make_conv2d(name="conv3_2", filters=256, kernel_size=3, strides=1, dilation_rate=1)(conv3_1)
    conv3_3 = make_conv2d(name="conv3_3", filters=256, kernel_size=3, strides=2, dilation_rate=1)(conv3_2)
    conv3_bn = make_batch_norm(name="norm3")(conv3_3)

    # -----CONV4-----
    conv4_1 = make_conv2d(name="conv4_1", filters=512, kernel_size=3, strides=1, dilation_rate=1)(conv3_bn)
    conv4_2 = make_conv2d(name="conv4_2", filters=512, kernel_size=3, strides=1, dilation_rate=1)(conv4_1)
    conv4_3 = make_conv2d(name="conv4_3", filters=512, kernel_size=3, strides=1, dilation_rate=1)(conv4_2)
    conv4_bn = make_batch_norm(name="norm4")(conv4_3)

    # -----CONV5-----
    conv5_1 = make_conv2d(name="conv5_1", filters=512, kernel_size=3, strides=1, dilation_rate=2)(conv4_bn)
    conv5_2 = make_conv2d(name="conv5_2", filters=512, kernel_size=3, strides=1, dilation_rate=2)(conv5_1)
    conv5_3 = make_conv2d(name="conv5_3", filters=512, kernel_size=3, strides=1, dilation_rate=2)(conv5_2)
    conv5_bn = make_batch_norm(name="norm5")(conv5_3)

    # -----CONV6-----
    conv6_1 = make_conv2d(name="conv6_1", filters=512, kernel_size=3, strides=1, dilation_rate=2)(conv5_bn)
    conv6_2 = make_conv2d(name="conv6_2", filters=512, kernel_size=3, strides=1, dilation_rate=2)(conv6_1)
    conv6_3 = make_conv2d(name="conv6_3", filters=512, kernel_size=3, strides=1, dilation_rate=2)(conv6_2)
    conv6_bn = make_batch_norm(name="norm6")(conv6_3)

    # -----CONV7-----
    conv7_1 = make_conv2d(name="conv7_1", filters=512, kernel_size=3, strides=1, dilation_rate=1)(conv6_bn)
    conv7_2 = make_conv2d(name="conv7_2", filters=512, kernel_size=3, strides=1, dilation_rate=1)(conv7_1)
    conv7_3 = make_conv2d(name="conv7_3", filters=512, kernel_size=3, strides=1, dilation_rate=1)(conv7_2)
    conv7_bn = make_batch_norm(name="norm7")(conv7_3)

    # -----CONV8-----
    conv8_1 = make_conv2d_transpose(name="deconv8_1", filters=256, kernel_size=4, strides=2, dilation_rate=1)(conv7_bn)
    conv8_2 = make_conv2d(name="conv8_2", filters=256, kernel_size=3, strides=1, dilation_rate=1)(conv8_1)
    conv8_3 = make_conv2d(name="conv8_3", filters=256, kernel_size=3, strides=1, dilation_rate=1)(conv8_2)
    conv8_313 = make_conv2d(name="conv8_313", filters=num_possible_bins, kernel_size=1, strides=1, dilation_rate=1)(conv8_3)

    return conv8_313
