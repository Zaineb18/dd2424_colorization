import os
import glob
import numpy as np

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Lambda, Input, Activation, Conv2D, Conv2DTranspose
from tensorflow.keras.regularizers import l2

from model.model_cnn import init_conv_net

IMAGE_DIM = 256


@tf.custom_gradient
def apply_prior_to_grad(x, prior):
    def grad(dy):
        return prior * dy, prior * 0

    return tf.identity(x), grad


def class_rebalance(tensors):
    x = tensors[0]
    prior = tensors[1]
    return apply_prior_to_grad(x, prior)


def class_rebalance_output_shape(input_shapes):
    return input_shapes[0]


def apply_prior(ab_quantized, prior):
    assert (len(ab_quantized.shape) == 4)
    prior = prior / np.sum(prior)

    gamma = 0.5

    Q = prior.shape[0]
    prior_mix = (1 - gamma) * prior + gamma / Q
    prior_factor = prior_mix ** -1
    prior_factor = prior_factor / np.sum(prior * prior_factor)  # re-normalize

    ab_maxidx = np.argmax(ab_quantized, axis=3)
    corr_factor = prior_factor[ab_maxidx]
    return corr_factor, prior_factor


def load_data(file_paths, samples_per_file, batch_start, batch_size, prior):
    num_bins = prior.shape[0]
    ab_quantized = np.zeros([batch_size, IMAGE_DIM // 4, IMAGE_DIM // 4, num_bins])
    lightness = np.zeros([batch_size, IMAGE_DIM, IMAGE_DIM, 1])

    file_idx = batch_start // samples_per_file
    idx_in_file = batch_start % samples_per_file
    print("IDX_IN_FILE " + str(idx_in_file))
    print("IDX_FILE " + str(file_idx))

    i = 0
    for file_idx in range(file_idx, len(file_paths)):
        with np.load(file_paths[file_idx]) as f:

            lightness_in_file = f["lightness"].astype(np.int32, copy=False)[:, :, :, np.newaxis]
            ab_quantized_in_file = f["binned_ab"].astype(np.float32, copy=False)

            samples_to_copy = min(batch_size - i, samples_per_file - idx_in_file)

            lightness[i:i+samples_to_copy] = lightness_in_file[idx_in_file:idx_in_file+samples_to_copy]
            ab_quantized[i:i+samples_to_copy] = ab_quantized_in_file[idx_in_file:idx_in_file+samples_to_copy]

            idx_in_file = 0
            i += samples_to_copy
            if i == batch_size:
                break

    #ab_quantized = ab_quantized[:, ::2, ::2, :]
    lightness = lightness - 50

    print("ab color shape: {}".format(ab_quantized.shape))
    print("lightness shape: {}".format(lightness.shape))

    corr_factor, prior_factor = apply_prior(ab_quantized, prior)
    corr_factor = corr_factor[:, :, :, np.newaxis]
    return ab_quantized, lightness, corr_factor


def load_data_generator(dir_str, batch_size):
    file_paths = glob.glob(os.path.join(dir_str, 'processed_test/*.npz'))
    samples_per_file = np.load(file_paths[0])["lightness"].shape[0]
    prior, _ = load_prior(dir_str)
    samples_total = samples_per_file * len(file_paths)
    batch_start = 0
    while True:
        ab_quantized, lightness, corr_factor = load_data(file_paths, samples_per_file, batch_start, batch_size, prior)
        print(lightness.shape[0])
        batch_start += lightness.shape[0]
        batch_start %= samples_total

        yield ([lightness, corr_factor], ab_quantized)


def load_prior(dir_str):
    prior = np.load(os.path.join(dir_str, "priors.npz"))["prior"]
    num_bins = prior.shape[0]
    return prior, num_bins


def init_model(num_possible_bins, train):
    # Inputs:
    img_input = Input(shape=(IMAGE_DIM, IMAGE_DIM, 1), name="image_input")
    prior_boost_input = Input(shape=(64, 64, 1), name='prior_boost_input')

    # ConvNet:
    conv_output = init_conv_net(img_input, num_possible_bins)

    if train:
        # Class Rebalance:
        class_rebalance_layer = Lambda(class_rebalance, output_shape=class_rebalance_output_shape, name="class_rebalance")
        class_rebalanced = class_rebalance_layer([conv_output, prior_boost_input])
        prediction = Activation('softmax')(class_rebalanced)
    else:
        annealed_output = Lambda(lambda x: x / 0.38, name="annealing")(conv_output)
        prediction = Activation('softmax')(annealed_output)

    model = Model(inputs=[img_input, prior_boost_input], outputs=prediction)
    op = tf.keras.optimizers.Adam(lr=3.16e-5, beta_1=0.9, beta_2=0.99, epsilon=None, decay=0.0, amsgrad=False)
    
    alpha = 0.001  # weight decay coefficient
    for layer in model.layers:
        if isinstance(layer, Conv2D) or isinstance(layer, Conv2DTranspose):
            layer.add_loss(l2(alpha)(layer.kernel))
        if hasattr(layer, 'bias_regularizer') and layer.use_bias:
            layer.add_loss(l2(alpha)(layer.bias))

    model.compile(optimizer=op,
                  loss='categorical_crossentropy',
                  metrics=['mean_squared_error', 'accuracy'])

    model.summary()
    return model
